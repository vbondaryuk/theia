﻿using Theia.Core.Models;

namespace Theia.Infrastructure.Models
{
    public class Rule : IRule
    {
        public string Source { get; set; }
    }
}